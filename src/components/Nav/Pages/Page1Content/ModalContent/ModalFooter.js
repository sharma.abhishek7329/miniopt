import React from "react";
import Button from "@mui/material/Button";
import {
  addPitListToLast,
  addCordinateListToLast,
  addRoadListToLast,
  addDumpListToLast,
} from "../../../../../store/Features/importReducer";
import { v4 as uuidv4 } from "uuid";
import { useSelector, useDispatch } from "react-redux";
const ModalFooter = ({
  title,
  newRow,
  clearPitInput,
  clearCordinateInput,
  clearDumpInput,
  clearRoadInput,
  closeModal,
}) => {
  const dispatch = useDispatch();
  let importItems = useSelector((store) => store.import);
  const addPitData = (newRow) => {

    clearPitInput();
    closeModal();
    let data = [];
    let newRowObj = {
      ...newRow,
    };

    data.push({
      name: "pits",
      fileName: "",
      id: uuidv4(),
      result: [newRowObj],
    });
    dispatch(addPitListToLast(data));
  };
  const addCordinateData = (newRow) => {

    clearCordinateInput();
    closeModal();
    let data = [];
    let newRowObj = {
      ...newRow,
    };
    data.push({
      name: "cordinates",
      fileName: "",
      id: uuidv4(),
      result: [newRowObj],
    });
    dispatch(addCordinateListToLast(data));
  };
  const addDumpData = (newRow) => {

    clearDumpInput();
    closeModal();
    let data = [];
    let newRowObj = {
      ...newRow,
    };
    data.push({
      name: "dumps",
      fileName: "",
      id: uuidv4(),
      result: [newRowObj],
    });
    dispatch(addDumpListToLast(data));
  };
  const addRoadData = (newRow) => {

    clearRoadInput();
    closeModal();
    let data = [];
    let newRowObj = {
      ...newRow,
    };
    data.push({
      name: "roads",
      fileName: "",
      id: uuidv4(),
      result: [newRowObj],
    });
    dispatch(addRoadListToLast(data));
  };
  const addData = (title, newRow) => {
    title === "Pit"
      ? addPitData(newRow)
      : title === "Cordinate"
        ? addCordinateData(newRow)
        : title === "Dump"
          ? addDumpData(newRow)
          : addRoadData(newRow);
  };
  return (
    <div className="modal-footer-container addModelBtn">
      <Button variant="contained" onClick={() => addData(title, newRow)}>
        Add {title}{" "}
      </Button>
    </div>
  );
};

export default ModalFooter;
