import React from "react";
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';


const ModalHeader = ({ title, closeModel}) => {
  return (
    <div className="modal-header-container">
      <div className="modal-title modal_title">{title}
        <div style={{paddingRight: '20px'}} onClick={closeModel}><CloseRoundedIcon></CloseRoundedIcon></div>
      </div>
    </div>
  ); 
};

export default ModalHeader;
