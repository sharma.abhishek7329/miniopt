import React, { useEffect } from "react";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import { useSelector, useDispatch } from "react-redux";
import TablePreview from "./TablePreview";
import SummaryPreview from "./SummaryPreview";
import { updateCurrTab } from "../../../../store/Features/importReducer";
const RightPanelPreview = ({ id }) => {
  let importItems = useSelector((store) => store.import);
  const dispatch = useDispatch();
  const [value, setValue] = React.useState("1");

  useEffect(() => {
    setValue(importItems.currPreview);
    dispatch(updateCurrTab(importItems.currPreview));
  }, [importItems.currPreview]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
    dispatch(updateCurrTab(newValue));
  };
  const dataPreview = (
    <div className="w-100">
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <TabList
            className="rightPrevTabList"
            onChange={handleChange}
            aria-label="lab API tabs example"
          >
            <Tab label="PIT" value="1" />
            <Tab label="COORDINATES" value="2" />
            <Tab label="DUMPS" value="3" />
            <Tab label="ROADS" value="4" />
          </TabList>
        </Box>
        <TabPanel className="w-100" value="1" style={{ padding: "0" }}>
          <TablePreview id={"pits"} list={importItems.pitList} />
        </TabPanel>
        <TabPanel className="w-100" value="2" style={{ padding: "0" }}>
          <TablePreview id={"cordinates"} list={importItems.cordinateList} />
        </TabPanel>
        <TabPanel className="w-100" value="3" style={{ padding: "0" }}>
          <TablePreview id={"dumps"} list={importItems.dumpList} />
        </TabPanel>
        <TabPanel className="w-100" value="4" style={{ padding: "0" }}>
          <TablePreview id={"roads"} list={importItems.roadList} />
        </TabPanel>
      </TabContext>
    </div>
  );
  return (
    <div className="w-100">
      {id === "dataPreviewWindow" ? (
        dataPreview
      ) : id === "summary" ? (
        <SummaryPreview data={importItems} currTab={value} />
      ) : (
        <div>No Preview Available</div>
      )}
    </div>
  );
};

export default RightPanelPreview;
