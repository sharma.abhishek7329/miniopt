import React from "react";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import { useSelector, useDispatch } from "react-redux";
const TableView = ({ header, id, list }) => {
  return (
    <Table
      stickyHeader
      size="small"
      aria-label="a dense table"
      style={{ height: "100%" }}
    >
      <TableHead>
        <TableRow className="table-header">
          {header.map((headerName, index) => {
            return (
              <TableCell key={index} className="table-cell">
                {headerName}
              </TableCell>
            );
          })}
        </TableRow>
      </TableHead>
      <TableBody>
        {list.map((items, index) => (
          <>
            {items.result.map((item, index) => {
              let datas = Object.values(item);

              const tabelCell = datas.map((data, index) => (
                <TableCell key={index} className="table-cell">
                  {data}
                </TableCell>
              ));
              return (
                <TableRow
                  className="table-header"
                  key={index}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  {tabelCell}
                </TableRow>
              );
            })}
          </>
        ))}
      </TableBody>
    </Table>
  );
};

export default TableView;
