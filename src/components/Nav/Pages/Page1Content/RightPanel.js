import React from "react";
import Grid from "@mui/material/Grid";
import Header from "./Header";
import { rightPanelObject } from "../Services/pageServices.js";
import RightPanelPreview from "./RightPanelPreview";
const RightPanel = () => {
  return (
    <>
      <Grid container spacing={2}>
        {rightPanelObject.map((item, key) => {

          return (
            <Grid item key={key} xs={12} className="gridItem">
              <Header
                title={item.name.toUpperCase()}
                sideButton={item.id === "summary" ? false : true}
                isArrow
                id={item.id}
              />
              <RightPanelPreview id={item.id} />
            </Grid>
          );
        })}
      </Grid>
    </>
  );
};

export default RightPanel;
