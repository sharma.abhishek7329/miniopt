import React, { useState, useEffect } from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { useSelector, useDispatch } from "react-redux";
import { addDumpData } from "../../../../../store/Features/importReducer";
const columns = [
  { id: "dumpNumber", label: "Pit/Dump" },
  { id: "totalOre", label: "Total Ore Tonnes" },
  { id: "totalWaste", label: "Total Waste Volume" },
  { id: "totalVolume", label: "Total Volume" },
];

export default function SummaryTableView() {
  let importItems = useSelector((store) => store.import);
  console.log(importItems, 'importItems');
  const dispatch = useDispatch();

  let totalOre = 0;
  let totalWaste = 0;
  let totalVolume = 0;
  let uniqueDump = [];
  let uniqueCordinate = [];
  let uniqueDumpHeader = [];
  let uniqueCordinateHeader = [];
  let newDumpList = [];
  let sum = 0;
  let totalDump = 0;
  let newDumpData = []


  importItems?.pitList[0]?.result.map((item, index) => {
    totalOre += Number(item[5]);
    totalWaste += Number(item[8]);
    totalVolume += Number(item[9]);
  });
  //pushing to unique dump array
  importItems?.dumpList[0]?.result.map((item, index) => {
    uniqueDump.push(item[0]);
  });
  //pushing to unique cordinate array
  importItems?.cordinateList[0]?.result.map((item, index) => {
    uniqueCordinate.push(item[0]);
  });

  if (uniqueDump.length > 0) {
    uniqueDumpHeader = [...new Set(uniqueDump)];
  }

  if (uniqueCordinate.length > 0) {
    uniqueCordinateHeader = [...new Set(uniqueCordinate)];
  }

  console.log('uniqueCordinate', uniqueCordinateHeader);


  if (uniqueDumpHeader.length > 0) {
    uniqueDumpHeader.map((header, headerIndex) => {

      let sum = 0;
      importItems?.dumpList[0]?.result.map((dumpitem, index) => {
        if (header === dumpitem[0]) {
          newDumpList[headerIndex] = sum += Number(dumpitem[9]);
        }
      });
    });
  }
  if (newDumpList.length > 0) {
    newDumpList.map((dumpList) => {
      totalDump += dumpList;
    });
  }


  return (
    <Paper className="table-container" sx={{ width: "100%" }}>
      <TableContainer sx={{ maxHeight: 440, overflow: "hidden" }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow className="table-header">
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  className="table-cell"
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow >
              <TableCell align="center" style={{ backgroundColor: 'rgb(93, 163, 250)', padding: '2px', color: 'white' }} colSpan={4} >
                {importItems.pitList.length > 0 ? `Pit Summary` : `Pit Summary(Not Available)`}
              </TableCell>
            </TableRow>
            {importItems.pitList.length > 0 && importItems.cordinateList.length === 0 &&
              (
                <TableRow className="table-header">
                  <TableCell className="table-cell">Pit</TableCell>
                  <TableCell className="table-cell">{totalOre}</TableCell>
                  <TableCell className="table-cell">{totalWaste}</TableCell>
                  <TableCell className="table-cell">{totalVolume}</TableCell>
                </TableRow>
              )
            }
            {importItems.pitList.length > 0 && importItems.cordinateList.length > 0 && uniqueCordinateHeader.map((header) =>
            (
              <TableRow className="table-header">
                <TableCell className="table-cell">{header}</TableCell>
                <TableCell className="table-cell">{totalOre}</TableCell>
                <TableCell className="table-cell">{totalWaste}</TableCell>
                <TableCell className="table-cell">{totalVolume}</TableCell>
              </TableRow>
            )
            )
            }
            <TableRow className="table-header">
              <TableCell className="table-cell">Total Pit</TableCell>
              <TableCell className="table-cell">{totalOre}</TableCell>
              <TableCell className="table-cell">{totalWaste}</TableCell>
              <TableCell className="table-cell">{totalVolume}</TableCell>
            </TableRow>
            <TableRow >
              <TableCell align="center" style={{ backgroundColor: 'rgb(93, 163, 250)', padding: '2px', color: 'white' }} colSpan={4} >{uniqueDumpHeader.length > 0 ? `Dump Summary` : `Dump Summary(Not Available)`}</TableCell>
            </TableRow>
            {uniqueDumpHeader.length > 0 &&
              uniqueDumpHeader.map((header, index) => {
                return (
                  <TableRow className="table-header" key={index}>
                    <TableCell className="table-cell">{header}</TableCell>
                    <TableCell className="table-cell">{`-`}</TableCell>
                    <TableCell className="table-cell">{`-`}</TableCell>
                    {newDumpList.length > 0 ? (
                      <TableCell className="table-cell">
                        {newDumpList[index]}
                      </TableCell>
                    ) : (
                      <TableCell className="table-cell">0</TableCell>
                    )}
                  </TableRow>
                );
              })}
            <TableRow className="table-header">
              <TableCell className="table-cell">Total Dumps</TableCell>
              <TableCell className="table-cell">{`0`}</TableCell>
              <TableCell className="table-cell">{`0`}</TableCell>
              <TableCell className="table-cell">{totalDump}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
}
