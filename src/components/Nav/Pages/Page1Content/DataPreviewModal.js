import React, { useState, useEffect } from "react";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import TextField from "@mui/material/TextField";
import { useSelector, useDispatch } from "react-redux";
import ModalHeader from "./ModalContent/ModalHeader";
import ModalFooter from "./ModalContent/ModalFooter";
import { updateDataModal } from "../../../../store/Features/modalReducer";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  // border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const DataPreviewModal = (props) => {
  let modalItems = useSelector((store) => store.modal);
  let importItems = useSelector((store) => store.import);
  const dispatch = useDispatch();
  const [newPitRow, setPitNewRow] = useState([
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  ]);
  const [newCordinateRow, setCordinateNewRow] = useState([
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  ]);
  const [newDumpRow, setDumpNewRow] = useState([
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  ]);
  const [newRoadRow, setRoadNewRow] = useState(["", "", "", "", ""]);

  const handlePitChange = (e, index) => {
    const _newRow = [...newPitRow];
    _newRow[index] = e.target.value;
    setPitNewRow(_newRow);
  };
  const handleCordinateChange = (e, index) => {
    const _newRow = [...newCordinateRow];
    _newRow[index] = e.target.value;
    setCordinateNewRow(_newRow);
  };
  const handleDumpChange = (e, index) => {
    const _newRow = [...newDumpRow];
    _newRow[index] = e.target.value;
    setDumpNewRow(_newRow);
  };
  const handleRoadChange = (e, index) => {
    const _newRow = [...newRoadRow];
    _newRow[index] = e.target.value;
    setRoadNewRow(_newRow);
  };

  const closeModal = () => {
    dispatch(updateDataModal(false));
  };
  const modalPreview = (tab) => {
    switch (tab) {
      case "1":
        return (
          <div className="modal-container">
            <div className="modal-header p3" style={{ paddingBottom: "0px" }}>
              <ModalHeader title={"Add Pit Data"} closeModel={closeModal} />
            </div>
            <div className="modal-body body_style">
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Block ID"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newPitRow[0]}
                  onChange={(e) => handlePitChange(e, 0)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Bench"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newPitRow[1]}
                  onChange={(e) => handlePitChange(e, 1)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Strip"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newPitRow[2]}
                  onChange={(e) => handlePitChange(e, 2)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Block"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newPitRow[3]}
                  onChange={(e) => handlePitChange(e, 3)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="P2O5"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newPitRow[4]}
                  onChange={(e) => handlePitChange(e, 4)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="ORE"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newPitRow[5]}
                  onChange={(e) => handlePitChange(e, 5)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Density"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newPitRow[6]}
                  onChange={(e) => handlePitChange(e, 6)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Recovery"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newPitRow[7]}
                  onChange={(e) => handlePitChange(e, 7)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Waste Volume"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newPitRow[8]}
                  onChange={(e) => handlePitChange(e, 8)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Total Volume"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newPitRow[9]}
                  onChange={(e) => handlePitChange(e, 9)}
                />
              </div>
            </div>
            <div className="modal-footer p3">
              <ModalFooter
                title={"Pit"}
                newRow={newPitRow}
                closeModal={closeModal}
                clearPitInput={() =>
                  setPitNewRow(["", "", "", "", "", "", "", "", "", ""])
                }
              />
            </div>
          </div>
        );
      case "2":
        return (
          <div className="modal-container">
            <div className="modal-header p3" style={{ paddingBottom: "0px" }}>
              <ModalHeader
                title={"Add Cordinate Data"}
                closeModel={closeModal}
              />
            </div>
            <div className="modal-body body_style">
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Pit"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newCordinateRow[0]}
                  onChange={(e) => handleCordinateChange(e, 0)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="Pit Id"
                  fullWidth
                  label="Bench"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newCordinateRow[1]}
                  onChange={(e) => handleCordinateChange(e, 1)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="X"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newCordinateRow[2]}
                  onChange={(e) => handleCordinateChange(e, 2)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Y"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newCordinateRow[3]}
                  onChange={(e) => handleCordinateChange(e, 3)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Z"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newCordinateRow[4]}
                  onChange={(e) => handleCordinateChange(e, 4)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Id"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newCordinateRow[5]}
                  onChange={(e) => handleCordinateChange(e, 5)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Bench"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newCordinateRow[6]}
                  onChange={(e) => handleCordinateChange(e, 6)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Strip"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newCordinateRow[7]}
                  onChange={(e) => handleCordinateChange(e, 7)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Block"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newCordinateRow[8]}
                  onChange={(e) => handleCordinateChange(e, 8)}
                />
              </div>
            </div>
            <div className="modal-footer p3">
              <ModalFooter
                title={"Cordinate"}
                newRow={newCordinateRow}
                closeModal={closeModal}
                clearCordinateInput={() =>
                  setCordinateNewRow(["", "", "", "", "", "", "", "", ""])
                }
              />
            </div>
          </div>
        );
      case "3":
        return (
          <div className="modal-container">
            <div className="modal-header p3" style={{ paddingBottom: "0px" }}>
              <ModalHeader title={"Add Dump Data"} closeModel={closeModal} />
            </div>
            <div className="modal-body body_style">
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Pit"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newDumpRow[0]}
                  onChange={(e) => handleDumpChange(e, 0)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Dump Id"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newDumpRow[1]}
                  onChange={(e) => handleDumpChange(e, 1)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Bench"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newDumpRow[2]}
                  onChange={(e) => handleDumpChange(e, 2)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Strip"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newDumpRow[3]}
                  onChange={(e) => handleDumpChange(e, 3)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Block"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newDumpRow[4]}
                  onChange={(e) => handleDumpChange(e, 4)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Dump Id"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newDumpRow[5]}
                  onChange={(e) => handleDumpChange(e, 5)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="X"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newDumpRow[6]}
                  onChange={(e) => handleDumpChange(e, 6)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Y"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newDumpRow[7]}
                  onChange={(e) => handleDumpChange(e, 7)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Z"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newDumpRow[8]}
                  onChange={(e) => handleDumpChange(e, 8)}
                />
              </div>
              <div className="modalInput">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Volume"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newDumpRow[9]}
                  onChange={(e) => handleDumpChange(e, 9)}
                />
              </div>
            </div>
            <div className="modal-footer p3">
              <ModalFooter
                title={"Dump"}
                newRow={newDumpRow}
                closeModal={closeModal}
                clearDumpInput={() =>
                  setDumpNewRow(["", "", "", "", "", "", "", "", "", ""])
                }
              />
            </div>
          </div>
        );
      case "4":
        return (
          <div className="modal-container">
            <div className="modal-header p3" style={{ paddingBottom: "0px" }}>
              <ModalHeader title={"Add Road Data"} closeModel={closeModal} s />
            </div>
            <div className="modal-body body_style4" style={{ height: "auto" }}>
              <div className="modalInput4">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Block"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newRoadRow[0]}
                  onChange={(e) => handleRoadChange(e, 0)}
                />
              </div>
              <div className="modalInput4">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Dump Id"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newRoadRow[1]}
                  onChange={(e) => handleRoadChange(e, 1)}
                />
              </div>
              <div className="modalInput4">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="X"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newRoadRow[2]}
                  onChange={(e) => handleRoadChange(e, 2)}
                />
              </div>
              <div className="modalInput4">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Y"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newRoadRow[3]}
                  onChange={(e) => handleRoadChange(e, 3)}
                />
              </div>
              <div className="modalInput4">
                <TextField
                  id="fullWidth"
                  fullWidth
                  label="Z"
                  type="text"
                  variant="standard"
                  size="small"
                  value={newRoadRow[4]}
                  onChange={(e) => handleRoadChange(e, 4)}
                />
              </div>
            </div>
            <div className="modal-footer p3">
              <ModalFooter
                title={"Road"}
                newRow={newRoadRow}
                clearRoadInput={() => setRoadNewRow(["", "", "", "", ""])}
                closeModal={closeModal}
              />
            </div>
          </div>
        );

      default:
        break;
    }
  };

  useEffect(() => {
    modalPreview(importItems.currTab);
  }, [importItems.currTab]);

  return (
    <div>
      <Modal open={modalItems.isDataModal} onClose={closeModal}>
        <Box sx={style}>{modalPreview(importItems.currTab)}</Box>
      </Modal>
    </div>
  );
};

export default DataPreviewModal;
