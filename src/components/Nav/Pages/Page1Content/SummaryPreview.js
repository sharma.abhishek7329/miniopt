import React from "react";
import { useSelector, useDispatch } from "react-redux";
import SummaryTableView from "./SummaryPage/SummaryTableView";
import {
  pitSummaryHeader,
  cordinateSummaryHeader,
} from "../Services/pageServices";
import { addDumpData } from "../../../../store/Features/importReducer";

const SummaryPreview = () => {
  let importItems = useSelector((store) => store.import);
  const dispatch = useDispatch();


  switch (importItems.currTab) {
    case "1":
      return <SummaryTableView />;
    case "2":
      return <SummaryTableView />;
    case "3":
      return <SummaryTableView />;
    case "4":
      return <SummaryTableView />;
    default:
      return <div>Something went wrong</div>;
  }
};

export default SummaryPreview;
