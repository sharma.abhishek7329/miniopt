import React from "react";
import Header from "./Header";
import Grid from "@mui/material/Grid";
import MiddlePanelContainer from "../Page2Content/MiddlePanelContainer";
const MiddlePanel = (props) => {
  // console.log("abhishek middle", props.allowMiddlewindow);
  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={12} className="gridItem">
          <Header title="Graphical flow drawing window" />
          {props.allowMiddlewindow && <MiddlePanelContainer />}
        </Grid>
      </Grid>
    </>
  );
};

export default MiddlePanel;
