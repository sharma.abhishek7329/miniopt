import React from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import { useSelector, useDispatch } from "react-redux";
import Delete from "@mui/icons-material/Delete";
import IconButton from "@mui/material/IconButton";
import { NoFileAdded } from "../../../../utils/util";
import {
  removePitListItem,
  removeCordinateListItem,
  removeDumpListItem,
  removeRoadListItem,
} from "../../../../store/Features/importReducer";
const ListPreview = ({ name }) => {
  let importItems = useSelector((store) => store.import);
  const dispatch = useDispatch();

  const dumpList =
    importItems.dumpList.length > 0 ? (
      importItems.dumpList.map((list) => {
        const { id, fileName } = list;
        return (
          <ListItem disablePadding key={id}>
            <ListItemButton>
              <ListItemText primary={fileName} className="textWrap" />
              <IconButton
                size="small"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                color="inherit"
                component="span"
                onClick={() => dispatch(removeDumpListItem(id))}
              >
                <Delete
                  fontSize="small"
                  sx={{ color: "red" }}

                />
              </IconButton>
            </ListItemButton>
          </ListItem>
        );
      })
    ) : (
      <NoFileAdded />
    );
  const pitList =
    importItems.pitList.length > 0 ? (
      importItems.pitList.map((list, index) => {
        const { id, fileName } = list;
        return (
          <ListItem disablePadding key={id}>
            <ListItemButton>
              <ListItemText primary={fileName} className="textWrap" />
              <IconButton
                size="small"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                color="inherit"
                component="span"
              // onClick={handleAdd}
              >
                <Delete
                  fontSize="small"
                  sx={{ color: "red" }}
                  onClick={() => dispatch(removePitListItem(id))}
                />
              </IconButton>
            </ListItemButton>
          </ListItem>
        );
      })
    ) : (
      <NoFileAdded />
    );
  const cordinateList =
    importItems.cordinateList.length > 0 ? (
      importItems.cordinateList.map((list, index) => {
        const { id, fileName } = list;
        return (
          <ListItem disablePadding key={id}>
            <ListItemButton>
              <ListItemText primary={fileName} className="textWrap" />
              <IconButton
                size="small"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                color="inherit"
                component="span"
              // onClick={handleAdd}
              >
                <Delete
                  fontSize="small"
                  sx={{ color: "red" }}
                  onClick={() => dispatch(removeCordinateListItem(id))}
                />
              </IconButton>
            </ListItemButton>
          </ListItem>
        );
      })
    ) : (
      <NoFileAdded />
    );
  const roadList =
    importItems.roadList.length > 0 ? (
      importItems.roadList.map((list, index) => {
        const { id, fileName } = list;
        return (
          <ListItem disablePadding key={id}>
            <ListItemButton>
              <ListItemText primary={fileName} className="textWrap" />
              <IconButton
                size="small"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                color="inherit"
                component="span"
              // onClick={handleAdd}
              >
                <Delete
                  fontSize="small"
                  sx={{ color: "red" }}
                  onClick={() => dispatch(removeRoadListItem(id))}
                />
              </IconButton>
            </ListItemButton>
          </ListItem>
        );
      })
    ) : (
      <NoFileAdded />
    );

  return (
    <div>
      <List className="fileList">
        {name === "dumps" ? (
          dumpList
        ) : name === "cordinates" ? (
          cordinateList
        ) : name === "pits" ? (
          pitList
        ) : name === "roads" ? (
          roadList
        ) : (
          <div>No List to display</div>
        )}
      </List>
    </div>
  );
};

export default ListPreview;
