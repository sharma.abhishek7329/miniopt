import React from "react";
import TableContainer from "@mui/material/TableContainer";
import Paper from "@mui/material/Paper";
import TableView from "./TableView";
import {
  pitHeader,
  cordinateHeader,
  dumpHeader,
  roadHeader,
} from "../Services/pageServices";

const pitTable = (id, list) => (
  <TableView header={pitHeader} id={id} list={list} />
);
const cordinateTable = (id, list) => (
  <TableView header={cordinateHeader} id={id} list={list} />
);
const dumpTable = (id, list) => (
  <TableView header={dumpHeader} id={id} list={list} />
);
const roadTable = (id, list) => (
  <TableView header={roadHeader} id={id} list={list} />
);

const TablePreview = ({ id, list }) => {
  return (
    <div
      style={{
        height: "100%",
      }}
    >
      <TableContainer className="table-container">
        {" "}
        {id === "pits" ? (
          pitTable(id, list)
        ) : id === "cordinates" ? (
          cordinateTable(id, list)
        ) : id === "dumps" ? (
          dumpTable(id, list)
        ) : id === "roads" ? (
          roadTable(id, list)
        ) : (
          <div> Something went wrong </div>
        )}{" "}
      </TableContainer>{" "}
    </div>
  );
};

export default TablePreview;
