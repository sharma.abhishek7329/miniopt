import React from "react";
import Grid from "@mui/material/Grid";
import Header from "./Header";
import { leftPanelObject } from "../Services/pageServices";
import ListPreview from "./ListPreview";


const LeftPanel = () => {
  return (
    <>
      <Grid container spacing={2}>
        {leftPanelObject.map((item, key) => {
          return (
            <Grid key={key} item xs={12} className="gridItem">
              <Header
                id={item.id}
                title={item.name.toUpperCase()}
                sideButton
                idprop={item.id}
                name={item.name}
              />
              <ListPreview name={item.name} />
            </Grid>
          );
        })}
      </Grid>
    </>
  );
};

export default LeftPanel;
