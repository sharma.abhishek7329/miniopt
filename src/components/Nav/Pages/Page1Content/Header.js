import React, { useRef, useState, useEffect } from "react";
import { updateDataModal } from "../../../../store/Features/modalReducer";
import AppBar from "@mui/material/AppBar";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import SaveIcon from "@mui/icons-material/Save";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import ArrowDownwardIcon from "@mui/icons-material/ArrowDownward";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import AccountCircle from "@mui/icons-material/AccountCircle";
import { styled } from "@mui/material/styles";
import { v4 as uuidv4 } from "uuid";
import DataPreviewModal from "./DataPreviewModal.js";
import { useSelector, useDispatch } from "react-redux";
import {
  addPits,
  addRoads,
  addDumps,
  addCordinates,
  addPitList,
  addCordinateList,
  addDumpList,
  addRoadList,
  clearRoadsList,
  clearPitsList,
  clearDumpsList,
  clearCordinatesList,
  updateCurrPreview,
} from "../../../../store/Features/importReducer";
const Header = (props) => {
  const { id, isArrow, name } = props;
  let importItems = useSelector((store) => store.import);
  let modalItems = useSelector((store) => store.modal);
  const dispatch = useDispatch();
  const inputFile = useRef(null);

  const handleAdd = () => {
    inputFile.current.click();
  };

  const handleDataPreview = () => {
    dispatch(updateDataModal(true));
  };

  const handleRemove = (id) => {
    if (id === "pits") {
      dispatch(clearPitsList());
    } else if (id === "cordinates") {
      dispatch(clearCordinatesList());
    } else if (id === "dumps") {
      dispatch(clearDumpsList());
    } else if (id === "roads") {
      dispatch(clearRoadsList());
    }
  };
  // const handleSave = (id) => {
  //   // console.log(id);
  // };
  // const handleDownArrow = (id) => {
  //   // console.log(id);
  // };
  // const handleUpArrow = (id) => {
  //   // console.log(id);
  // };

  const processCSV = (str, delim = ",") => {
    const headers = str.slice(0, str.indexOf("\n")).split(delim);
    const rows = str.slice(str.indexOf("\n") + 1).split("\n");

    const newArray = rows.map((row) => {
      const values = row.split(delim);
      const eachObject = headers.reduce((obj, header, i) => {
        obj[i] = values[i];
        return obj;
      }, {});
      return eachObject;
    });
    return newArray;
  };

  const onChangePits = async (files, name) => {
    // files = e.target.file[0]
    const reader = new FileReader();
    const _pit = [];
    // _pit.push({"name": name , "fileName": files.name , "id":uuidv4() , 'result' : result});
    reader.onload = function (evt) {
      const text = evt.target.result;
      const result = processCSV(text);
      result.pop();
      // console.log("abhishek result", result);
      //conversion of 6 element of array into decimal
      //----------------conversion here-------------------------
      result.map((item, index) => {
        item[6] = parseFloat(item[6]).toFixed(2);
        return item;
      });
      //----------------conversion here-------------------------
      _pit.push({
        name: name,
        fileName: files.name,
        id: uuidv4(),
        result: result,
      });
      dispatch(addPitList(_pit));
    };
    reader.readAsText(files);
  };
  const onChangeCordinates = (files, name) => {
    const reader = new FileReader();
    const _coordinate = [];

    reader.onload = function (evt) {
      const text = evt.target.result;
      const result = processCSV(text);
      result.pop();
      _coordinate.push({
        name: name,
        fileName: files.name,
        id: uuidv4(),
        result: result,
      });
      dispatch(addCordinateList(_coordinate));
    };
    reader.readAsText(files);
  };
  const onChangeRoads = (files, name) => {
    const reader = new FileReader();
    const _roads = [];
    // _roads.push({"name": name , "fileName": files.name , "id":uuidv4()});
    reader.onload = function (evt) {
      const text = evt.target.result;
      const result = processCSV(text);
      result.pop();
      _roads.push({
        name: name,
        fileName: files.name,
        id: uuidv4(),
        result: result,
      });
      dispatch(addRoadList(_roads));
    };
    reader.readAsText(files);
  };

  const onChangeDumps = (files, name) => {
    const reader = new FileReader();
    const _dumps = [];
    reader.onload = function (evt) {
      const text = evt.target.result;
      const result = processCSV(text);
      result.pop();
      _dumps.push({
        name: name,
        fileName: files.name,
        id: uuidv4(),
        result: result,
      });
      dispatch(addDumpList(_dumps));
    };
    reader.readAsText(files);
  };

  const handleInputChange = (e) => {
    switch (e.target.id) {
      case "pits":
        dispatch(updateCurrPreview("1"));
        return onChangePits(e.target.files[0], "pits");
      case "cordinates":
        dispatch(updateCurrPreview("2"));
        return onChangeCordinates(e.target.files[0], "cordinates");
      case "dumps":
        dispatch(updateCurrPreview("3"));
        return onChangeDumps(e.target.files[0], "dumps");
      case "roads":
        dispatch(updateCurrPreview("4"));
        return onChangeRoads(e.target.files[0], "roads");
      default:
        return alert("default input selected");
    }
  };

  return (
    <>
      <AppBar
        position="static"
        style={{ height: "15px !important", backgroundColor: "#5DA3FA" }}
      >
        <Toolbar id="header" variant="dense">
          <Typography
            component={"span"}
            variant={"body2"}
            sx={{ flexGrow: 1 }}
            className="headerTitle"
            style={
              !props.sideButton
                ? {
                  // textAlign: "center",
                  height: "20px",
                  fontSize: "14px",
                }
                : { height: "20px", fontSize: "14px" }
            }
          >
            {props.title}
          </Typography>
          {props?.sideButton && (
            <>
              <div>
                <label htmlFor="icon-button-file">
                  <input
                    type="file"
                    id={`${id}`}
                    ref={inputFile}
                    onChange={handleInputChange}
                    style={{ display: "none" }}
                  />
                  {/* <Input id={`${id}`} type="file" multiple onChange={(e) => handleInputChange(e , id)} /> */}
                  <IconButton
                    size="small"
                    aria-label="account of current user"
                    aria-controls="menu-appbar"
                    aria-haspopup="true"
                    color="inherit"
                    component="span"
                    onClick={() => {
                      if (id === "dataPreviewWindow") {
                        handleDataPreview();
                      } else {
                        handleAdd();
                      }
                    }}
                  >
                    <AddIcon />
                  </IconButton>
                </label>
              </div>
              <IconButton
                size="small"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={() => handleRemove(id)}
                color="inherit"
              >
                <RemoveIcon />
              </IconButton>
              {/* <IconButton
                size="small"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={()=>handleSave(id)}
                color="inherit"
              >
                <SaveIcon />
              </IconButton> */}
              {isArrow && (
                <>
                  {/* <IconButton
                    size="small"
                    aria-label="account of current user"
                    aria-controls="menu-appbar"
                    aria-haspopup="true"
                    onClick={() => handleDownArrow(id)}
                    color="inherit"
                  >
                    <ArrowDownwardIcon />
                  </IconButton>
                  <IconButton
                    size="small"
                    aria-label="account of current user"
                    aria-controls="menu-appbar"
                    aria-haspopup="true"
                    onClick={() => handleUpArrow(id)}
                    color="inherit"
                  >
                    <ArrowUpwardIcon />
                  </IconButton> */}
                </>
              )}
            </>
          )}
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Header;
