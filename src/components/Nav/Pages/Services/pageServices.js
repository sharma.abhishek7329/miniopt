export const leftPanelObject = [
  {
    id: "pits",
    name: "pits",
  },
  {
    id: "cordinates",
    name: "cordinates",
  },
  {
    id: "dumps",
    name: "dumps",
  },
  {
    id: "roads",
    name: "roads",
  },
];
export const rightPanelObject = [
  {
    id: "dataPreviewWindow",
    name: "Data Preview Window",
  },
  {
    id: "summary",
    name: "Summary",
  },
];
// Block_Id,Bench,Strip,Block,P2O5(Quality),Ore(Tons),Density,Recovery,Waste Vol,Total Volume
export const pitHeader = [
  "Block ID",
  "Bench",
  "Strip",
  "Block",
  "P2O5(Quality)",
  "Ore(Tons)",
  "Density",
  "Recovery",
  "Waste Volume",
  "Total Volume",
];
// ;Pit,Pit Id,X,Y,Z,id,Bench,Strip,Block
export const cordinateHeader = [
  "Pit",
  "Pit Id",
  "X",
  "Y",
  "Z",
  "Id",
  "Bench",
  "Strip",
  "Block",
];
// Pit,Dump id,Bench,Strip,Block,Dump Id,x,y,z,VOLW
export const dumpHeader = [
  "Pit",
  "Dump Id",
  "Bench",
  "Strip",
  "Block",
  "Dump Id",
  "X",
  "Y",
  "Z",
  "Volume",
];
//Pt No,Id,x,y,z
export const roadHeader = ["Pit Number", "Id", "X", "Y", "Z"];

export const pitSummaryHeader = [
  { id: "dumpNumber", label: "Pit/Dump Number" },
  { id: "totalOre", label: "Total Ore Tonnes" },
  { id: "totalWaste", label: "Total Waste Volume" },
  { id: "totalVolume", label: "Total Volume" },
];

export const cordinateSummaryHeader = [
  { id: "dumpNumber", label: "Pit/Dump Number" },
  { id: "totalVolume", label: "Total Volume" },
];
