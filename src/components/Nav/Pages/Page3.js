import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { addMillCordinateDataMax, addMillCordinateDataMin } from '../../../store/Features/importReducer'
// Note: Uncomment import lines in your project.
// import React from 'react';
// import ReactDOM from 'react-dom';

// CELL -----------------------------------------

const itemStyle = {
  padding: "0px",
  position: "relative",
  height: "26px",
  display: "flex",
  alignItems: "center",
};

const textStyle = {
  ...itemStyle,
  padding: "0px 4px",
  height: "26px",
  fontFamily: "Arial",
  fontSize: "13px",
};

const inputStyle = {
  padding: "2px",
  // position: "absolute",
  // left: "2px",
  // top: "2px",
  // right: "45px",
  // bottom: "2px",
  minWidth: "20px",
  fontFamily: "Arial",
  fontSize: "13px",
  width: '100%'
};

const buttonStyle = {
  position: "absolute",
  top: "2px",
  right: "2px",
  bottom: "2px",
  width: "40px",
};

const Cell = React.memo(({ value, mode, onChange, path }) => {
  const [localMode, setLocalMode] = React.useState(mode ?? "read");
  const [localValue, setLocalValue] = React.useState(value ?? "");
  React.useEffect(() => setLocalMode(mode ?? "read"), [mode]);
  React.useEffect(() => setLocalValue(value ?? ""), [value]);
  if (localMode === "edit") {
    const handleInputChange = (e) => {
      setLocalValue(e.target.value)
      // setLocalMode("read");
      onChange?.(e.target.value);
    };
    return (
      <div style={itemStyle}>
        <input
          type="text"
          value={localValue}
          style={inputStyle}
          onChange={handleInputChange}
          onBlur={() => setLocalMode("read")}
        />
        {/* <button style={buttonStyle} onClick={handleSaveClick}>
          Ok
        </button> */}
      </div>
    );
  }
  if (localMode === "read") {
    const handleEditClick = () => {
      if (path === 'stockPileNumber') {
        setLocalMode("read");
      }
      else {
        setLocalMode("edit");
      }
    };
    return (
      <div style={textStyle} onClick={handleEditClick}>
        {localValue}
      </div>
    );
  }
  return null;
});

// ROW ------------------------------------------

const tdStyle = {
  padding: "1px",
  border: "1px solid black",
};

const Row = React.memo(({ mode, columns, data, onChange, onDelete }) => {
  const handleDeleteClick = () => onDelete?.();

  return (
    <tr>
      {columns.map(({ path }, columnIndex) => {
        const handleChange = (value) => {
          if (onChange) {
            const changedData = { ...data, [path]: value };
            onChange(columnIndex, changedData);
          }
        };
        return (
          <td key={path} style={tdStyle}>
            <Cell mode={mode} value={data[path]} onChange={handleChange} path={path} />
          </td>
        );
      })
      }
      {/* <td style={optionStyle}>
        <button onClick={handleDeleteClick}>Delete</button>
      </td> */}
    </tr>
  );
});

// TABLE ----------------------------------------

const tableStyle = {
  border: "1px solid black",
  borderCollapse: "collapse",
  width: "100%",
};

const Table = React.memo(({ id, columns, data, cordinate, onAdd, onChange, onDelete }) => {

  const [addedIndex, setAddedIndex] = React.useState();
  const cordinateDataArray = ['X', 'Y', 'Z']

  const handleAddClick = () => {
    onAdd?.(data.length);
    setAddedIndex(data.length);
  };

  return (
    <div style={{ width: '100%', height: '195px', overflow: 'auto' }} >
      <table style={tableStyle}>
        <tbody>
          <tr>
            {columns.map(({ path, name }) => (
              <th key={path}
                // style={path === 'capacity' ? capStyle : tdStyle}
                style={tdStyle}
              >
                {name}
              </th>
            ))}
          </tr>
          <tr>
            <th style={tdStyle}></th>
            {
              cordinateDataArray.map(item => <th style={tdStyle}>{`${item}`}</th>)
            }
          </tr>
          {data.map((rowData, rowIndex) => {
            const handleChange = (columnIndex, changedData) => {
              onChange?.(rowIndex, columnIndex, changedData);
            };
            const handleDelete = () => {
              if (rowIndex !== addedIndex) {
                setAddedIndex(addedIndex - 1);
              }
              onDelete?.(rowIndex, rowData);
            };
            return (
              <Row
                key={rowData[id]}
                mode={addedIndex === rowIndex ? "edit" : "read"}
                columns={columns}
                data={rowData}
                onChange={handleChange}
                onDelete={handleDelete}
              />
            );
          })}
        </tbody>
      </table>
      {/* <div>
        <button onClick={handleAddClick}>Add row</button>
      </div> */}
    </div>
  );
});

// UTILS ----------------------------------------

// https://dirask.com/snippets/React-append-prepend-remove-and-replace-items-in-array-with-utils-for-useState-D7XEop

const appendItem = (updater, item) => {
  updater((array) => array.concat(item));
};

const replaceItem = (updater, index, item) => {
  updater((array) => array.map((value, i) => (i === index ? item : value)));
};

const deleteItem = (updater, index) => {
  updater((array) => array.filter((value, i) => i !== index));
};

// Example --------------------------------------

const columns = [
  { path: "stockPileNumber", name: "Stock Pile Number" },
];

let counter = 0;
const Page3 = (props) => {
  let importItems = useSelector((store) => store.import);
  const dispatch = useDispatch();
  let manupulatedColumns = [...columns]
  const cordinateDataArray = ['X', 'Y', 'Z']
  cordinateDataArray.map((item) => {
    manupulatedColumns.push({ path: `${item}`, name: "" })
  })
  // const stockData = []
  // importItems.processPlantList.map((item, index) => {
  //     stockData.push({ id: index + 1, stockPileNumber: item.symbol })
  // })
  const [data, setData] = useState([]);

  useEffect(() => {
    let modifiedData = [...data]
    importItems.processPlantList.map((item, index) => {
      modifiedData[importItems.processPlantList.length - 1] = { id: index + 1, stockPileNumber: item.symbol }
    })
    setData(modifiedData)
  }, [importItems.processPlantList]);


  useEffect(() => {
    if (props.min) {
      dispatch(addMillCordinateDataMin(data))
    } else {
      dispatch(addMillCordinateDataMax(data))
    }
  }, [data]);

  useEffect(() => {
    if (importItems.millCordinateDataMin.length > 0 && props.min) {
      setData(importItems.millCordinateDataMin[0])
    }
    else if (importItems.millCordinateDataMax.length > 0 && props.max) {
      setData(importItems.millCordinateDataMax[0])
    }
  }, []);

  const handleAdd = (rowIndex) => {
    const newRowData = { id: ++counter };
    appendItem(setData, newRowData);
    //TODO: AJAX request to server
  };
  const handleChange = (rowIndex, columnIndex, changedRowData) => {
    replaceItem(setData, rowIndex, changedRowData);
    const changedRowJson = JSON.stringify(changedRowData, null, 4);
    //TODO: AJAX request to server
  };
  const handleDelete = (rowIndex, deletedRowData) => {
    deleteItem(setData, rowIndex);
    //TODO: AJAX request to server
  };
  return (
    <div className="page3" >
      <Table
        id="id"
        columns={manupulatedColumns}
        data={data}
        onAdd={handleAdd}
        cordinate={3}
        onChange={handleChange}
        onDelete={handleDelete}
      />
    </div>
  );
};

export default Page3;

