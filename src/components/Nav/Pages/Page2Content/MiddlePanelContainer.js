import { ConstructionOutlined } from "@mui/icons-material";
import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { addDumpData } from "../../../../store/Features/importReducer";
import MiddlePanelImage from "./MiddlePanelImage";
const MiddlePanelContainer = () => {
  let importItems = useSelector((store) => store.import);
  console.log(importItems, 'importItems');
  const dispatch = useDispatch();
  const [dumpData, setDumpData] = useState([]);


  useEffect(() => {
    console.log('importItems.dumpList', importItems.dumpList);
    let allDumpNumbers = [];
    let uniqueDumpHeader = [];
    let newDumpData = [];
    importItems?.dumpList[0]?.result.map((item, index) => {
      allDumpNumbers.push(item[0]);
    });
    if (allDumpNumbers.length > 0) {
      uniqueDumpHeader = [...new Set(allDumpNumbers)];
    }
    if (uniqueDumpHeader.length > 0) {
      uniqueDumpHeader.map((item, index) => {
        newDumpData.push({
          id: index + 1,
          symbol: `D${index + 1}`,
          name: `Dump${item}`,
          type: 'internal'
        })
      })
      dispatch(addDumpData(newDumpData))
    }
  }, [importItems.dumpList]);



  return (
    <div style={{ width: "100%", height: "100vh" }}>
      {/* {importItems.pitList.map((item) => {
        return (
          <MiddlePanelImage
            key={item.id}
            imageRender="pit"
            data={importItems.pitList}
          />
        );
      })} */}
      {importItems.stockPileList.map((item) => {
        return (
          <MiddlePanelImage imageRender="stockPile" key={item.id} data={item} />
        );
      })}
      {importItems.uniqueDumpData.length > 0 && importItems.uniqueDumpData.map((item) => {
        console.log('unique item', item)
        return (
          <MiddlePanelImage
            key={item.id}
            imageRender="dump"

            data={item}
          />
        );
      })}
      {importItems.processPlantList.map((item) => {
        return (
          <MiddlePanelImage
            imageRender="processPlant"
            key={item.id}
            data={item}
          />
        );
      })}
    </div>
  );
};

export default MiddlePanelContainer;
