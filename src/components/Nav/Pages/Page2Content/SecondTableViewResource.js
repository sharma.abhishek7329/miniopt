
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { addResourceData, addStockPileData } from '../../../../store/Features/importReducer'
import swal from 'sweetalert';
import { useToast } from 'izitoast-react'
import 'izitoast-react/dist/iziToast.css';

const itemStyle = {
    padding: "0px",
    position: "relative",
    height: "26px",
    display: "flex",
    alignItems: "center",
    justifyContent: 'center'
};

const textStyle = {
    ...itemStyle,
    padding: "0px 4px",
    height: "26px",
    fontFamily: "Arial",
    fontSize: "13px",
};

const inputStyle = {
    padding: "2px",
    // position: "absolute",
    // left: "2px",
    // top: "2px",
    // right: "45px",
    // bottom: "2px",
    minWidth: "20px",
    fontFamily: "Arial",
    fontSize: "13px",
    width: '100%'
};


const Cell = React.memo(({ value, mode, onChange, path, disable, year, data, updateData, showMessage }) => {

    const [localMode, setLocalMode] = React.useState(mode ?? "read");
    const [localValue, setLocalValue] = React.useState(value ?? "");

    React.useEffect(() =>
        setLocalMode(mode ?? "read")
        , [mode]);
    React.useEffect(() => {
        setLocalValue(value ?? "")
    }, [value]);


    if (localMode === "edit") {
        const handleInputChange = (e) => {
            console.log(data, 'add alert');
            if (data.capacity === true) {
                swal({
                    icon: "info",
                    text: "Value cannot be changed on repeat mode",
                });
                return;
            }
            setLocalValue(e.target.value)
            onChange?.(e.target.value);
        };
        return (
            <div style={itemStyle}>
                <input
                    type="text"
                    value={localValue}
                    style={inputStyle}
                    onChange={handleInputChange}
                    onBlur={() => setLocalMode("read")}
                />
            </div>
        );
    }
    if (localMode === "read") {
        const handleInputChange = (e) => {
            if (e.target.value === 'true') {
                showMessage()
                setLocalValue(e.target.checked)
                onChange?.(e.target.checked);
            }
            else {
                if (e.target.checked) {
                    swal({
                        title: "Are you sure ??",
                        text: "Values will be repeated for all years",
                        cancel: true,
                        buttons: true,
                    }).then(state => {
                        if (state === null) {
                            setLocalValue(!e.target.checked)
                            onChange?.(!e.target.checked);
                        }
                        else {
                            setLocalValue(e.target.checked)
                            onChange?.(e.target.checked);
                        }
                    })
                }
            }


        };

        const handleEditClick = () => {
            if (path === 'resourceNumber' || path === 'capacity') {
                setLocalMode("read");
            }
            else {
                setLocalMode("edit");
            }
        };
        if (path === 'capacity') {
            console.log('localValue', localValue);
            return (<div style={itemStyle}>
                <input
                    type="checkbox"
                    value={localValue}
                    onChange={handleInputChange}
                    disabled={disable}
                    defaultChecked={localValue}
                />
                <label htmlFor=""
                    className={disable === true ? 'disabledLabel' : ""}
                >Repeat</label>
            </div>)
        }
        else {
            return (
                <div style={textStyle} onClick={handleEditClick}>
                    {localValue}
                </div>
            );
        }


    }

    return null;
});

// ROW ------------------------------------------

const tdStyle = {
    padding: "1px",
    border: "1px solid black",
};

const Row = React.memo(({ mode, columns, data, year, onChange, onDelete, showMessage }) => {
    const handleDeleteClick = () => onDelete?.();
    const [disableCheckBox, setDisableCheckBox] = useState(data.disableRepeat)

    useEffect(() => {
        if (data.year1 && data.year1 !== '') {
            setDisableCheckBox(false)
        }
        if (data.year1 && data.year1 === '') {
            setDisableCheckBox(true)
        }

        return () => {
            setDisableCheckBox(true)
        };
    }, [data])


    const handleUpdateData = (param) => {
        // console.log(param);
    }


    return (
        <tr>
            {columns.map(({ path }, columnIndex) => {
                const handleChange = (value) => {
                    if (onChange) {
                        const changedData = { ...data, [path]: value };
                        onChange(columnIndex, changedData);
                    }
                };
                return (
                    <td key={path} style={tdStyle}>
                        <Cell mode={mode} value={data[path]} onChange={handleChange} path={path} disable={disableCheckBox} year={year} data={data} updateData={handleUpdateData} showMessage={showMessage} />
                    </td>
                );
            })
            }
        </tr>
    );
});

// TABLE ----------------------------------------

const tableStyle = {
    border: "1px solid black",
    borderCollapse: "collapse",
    width: "100%",
};

const Table = React.memo(({ id, columns, data, year, onAdd, onChange, onDelete, showMessage }) => {

    const [addedIndex, setAddedIndex] = React.useState();
    const yearDataArray = Array.from({ length: year }, (_, i) => i + 1)

    const handleAddClick = () => {
        onAdd?.(data.length);
        setAddedIndex(data.length);
    };

    return (
        <div style={{ width: '100%', height: '195px', overflow: 'auto' }} >
            <table style={tableStyle}>
                <tbody>
                    <tr>
                        {columns.map(({ path, name }) => (
                            <th key={path}
                                style={tdStyle}
                            >
                                {name}
                            </th>
                        ))}
                    </tr>
                    <tr>
                        <th style={tdStyle}></th>
                        <th style={tdStyle}></th>
                        {
                            yearDataArray.map(item => <th style={tdStyle}>{`Year ${item}`}</th>)
                        }
                    </tr>
                    {data.map((rowData, rowIndex) => {
                        const handleChange = (columnIndex, changedData) => {
                            onChange?.(rowIndex, columnIndex, changedData);
                        };
                        const handleDelete = () => {
                            if (rowIndex !== addedIndex) {
                                setAddedIndex(addedIndex - 1);
                            }
                            onDelete?.(rowIndex, rowData);
                        };
                        return (
                            <Row
                                key={rowData[id]}
                                mode={addedIndex === rowIndex ? "edit" : "read"}
                                columns={columns}
                                data={rowData}
                                year={yearDataArray}
                                onChange={handleChange}
                                onDelete={handleDelete}
                                showMessage={showMessage}
                            />
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
});


const appendItem = (updater, item) => {
    updater((array) => array.concat(item));
};

const replaceItem = (updater, index, item) => {
    updater((array) => array.map((value, i) => (i === index ? item : value)));
};

const deleteItem = (updater, index) => {
    updater((array) => array.filter((value, i) => i !== index));
};

const columns = [
    { path: "resourceNumber", name: "Resource" },
    { path: "capacity", name: "Max Capacity" },
];

let counter = 0;
const SecondTableViewResource = () => {

    let importItems = useSelector((store) => store.import);
    const dispatch = useDispatch();
    let manupulatedColumns = [...columns]
    const yearDataArray = Array.from({ length: importItems.currYear }, (_, i) => i + 1)

    yearDataArray.map((item) => {
        manupulatedColumns.push({ path: `year${item}`, name: "" })
    })
    const millData = [
        { id: 1, resourceNumber: `Resource Capacity(Min)` },
        { id: 2, resourceNumber: `Resource Capacity(Max)` },
    ]
    const [data, setData] = useState(millData);
    const showMessage = useToast({
        message: 'Values can be edited',
        theme: 'light', // dark
        color: 'green', // blue, red, green, yellow
    });


    useEffect(() => {
        console.log(data, importItems.processPlantList, 'firstdata');
        let modifiedData = [...data]
        setData(modifiedData)
    }, [data]);




    useEffect(() => {
        console.log('data enter', data);
        if (data.length > 0) {
            data.map((item, index) => {
                console.log(item, "data item");
                let newData = []
                let obj = {}
                if (item?.capacity === true) {
                    yearDataArray.forEach(element => {
                        console.log(element, "data item eleemnt");
                        obj[`year${element}`] = item.year1
                        console.log('data obj', obj);
                        newData.push(obj)
                    })
                }
                else {
                    newData.push(obj)
                }
                let finalData = [{ ...data[index], ...newData[0] }]
                console.log('finalData', finalData);
                data.splice(item.id - 1, 1, finalData[0])
                console.log('final data here', finalData);
            })
        }
        console.log('data', data);
        setData(data)
        dispatch(addResourceData(data))
    }, [data]);

    useEffect(() => {
        console.log('coming here', importItems);
        if (importItems.resourceData.length > 0) {
            setData([...importItems?.resourceData[0]])
        }
    }, []);

    useEffect(() => {
        console.log('coming here', importItems);
        if (importItems.resourceData.length > 0) {
            setData([...importItems?.resourceData[0]])
        }
    }, [importItems.secondCurrPreview]);

    const handleAdd = (rowIndex) => {
        const newRowData = { id: ++counter };
        appendItem(setData, newRowData);
        //TODO: AJAX request to server
    };
    const handleChange = (rowIndex, columnIndex, changedRowData) => {
        replaceItem(setData, rowIndex, changedRowData);
        const changedRowJson = JSON.stringify(changedRowData, null, 4);
        //TODO: AJAX request to server
    };
    const handleDelete = (rowIndex, deletedRowData) => {
        deleteItem(setData, rowIndex);
        //TODO: AJAX request to server
    };

    return (
        <div >
            <Table
                id="id"
                columns={manupulatedColumns}
                data={data}
                onAdd={handleAdd}
                year={importItems.currYear}
                onChange={handleChange}
                onDelete={handleDelete}
                showMessage={showMessage}
            />
        </div>
    );
};

export default SecondTableViewResource;

