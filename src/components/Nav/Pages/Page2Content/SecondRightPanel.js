import React from "react";
import SecondRightContainer from "./SecondRightContainer";

const SecondRightPanel = () => {
  return <SecondRightContainer />;
};

export default SecondRightPanel;
