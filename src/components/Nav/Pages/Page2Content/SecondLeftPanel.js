import React, { useEffect, useState } from "react";
import stockImage from "../../../../assets/images/Add StockPile.png";
import plantImage from "../../../../assets/images/Add ProcessPlant.png";

import Divider from "@mui/material/Divider";
import SecondLeftContainer from "./SecondLeftContainer";
import YearDisplayView from "./YearDisplayView";
import { useDispatch, useSelector } from "react-redux";

const SecondLeftPanel = () => {
  let importItems = useSelector((store) => store.import);
  const dispatch = useDispatch();
  const [disableStock, setDisableStock] = useState(false);
  const [disableMill, setDisableMill] = useState(false);

  useEffect(() => {
    console.log(importItems.secondCurrPreview, 'importItemspreview');
    if (importItems.secondCurrPreview === '1') {
      setDisableStock(false)
      setDisableMill(true)
    }
    else if (importItems.secondCurrPreview === '2') {
      setDisableStock(true)
      setDisableMill(false)
    }
    else {
      setDisableStock(true)
      setDisableMill(true)
    }
    // if (importItems.secondCurrPreview === '1' && id) { }
  }, [importItems.secondCurrPreview])

  return (
    <div className="secondLeftPanelContainer">
      <YearDisplayView />
      <Divider />
      <SecondLeftContainer
        title="Add Stockpiles"
        sourceImage={stockImage}
        alt="stockimage"
        id="stockpile"
        disable={disableStock}
      />
      <Divider />
      <SecondLeftContainer
        title="Add Process Plants"
        sourceImage={plantImage}
        alt="plantImage"
        id="processPlant"
        disable={disableMill}
      />
      <Divider />
    </div>
  );
};

export default SecondLeftPanel;
