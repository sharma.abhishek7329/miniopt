import React, { useEffect } from "react";
import ButtonGroup from "@mui/material/ButtonGroup";
import Button from "@mui/material/Button";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import { useSelector, useDispatch } from "react-redux";
import {
  addStockPileList,
  addProcessPlantList,
  updateSecondCurrPreview,
} from "../../../../store/Features/importReducer";
import { v4 as uuidv4 } from "uuid";
const SecondLeftContainer = ({ title, sourceImage, alt, id, disable }) => {
  let importItems = useSelector((store) => store.import);
  const dispatch = useDispatch();

  const addStockPile = (idStock) => {
    let data = {
      uid: uuidv4(),
      name: `Stock Pile ${importItems.stockPileList.length + 1}`,
      symbol: `S${importItems.stockPileList.length + 1}`
    };
    dispatch(addStockPileList(data));
  };
  const addProcessPlant = (idStock) => {

    let data = {
      uid: uuidv4(),
      name: `Mill ${importItems.processPlantList.length + 1}`,
      symbol: `M${importItems.processPlantList.length + 1}`
    };
    dispatch(addProcessPlantList(data));
  };
  const removeStockPile = (id) => {

  };
  const removeProcessPlant = (id) => {

  };



  return (
    <div className="secondLeftInnerContainer">
      <p>{title}</p>
      <img className="secondLeftPanelImage" src={sourceImage} alt={alt} />
      <ButtonGroup disableElevation variant="contained">
        <Button
          onClick={() => {
            id === "stockpile" ? (
              addStockPile(id)
            ) : id === "processPlant" ? (
              addProcessPlant(id)
            ) : (
              <div>Something went wrong</div>
            );
          }}
          disabled={disable}
        >
          <AddIcon />
        </Button>
        {/* <Button
          onClick={() => {
            id === "stockpile" ? (
              removeStockPile(id)
            ) : id === "processPlant" ? (
              removeProcessPlant(id)
            ) : (
              <div>Something went wrong</div>
            );
          }}
        >
          <RemoveIcon />
        </Button> */}
      </ButtonGroup>
    </div>
  );
};

export default SecondLeftContainer;
