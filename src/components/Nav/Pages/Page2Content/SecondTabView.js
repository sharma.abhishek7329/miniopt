import React, { useEffect } from "react";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import SecondTableViewStock from "./SecondTableViewStock";
import SecondTableViewMill from "./SecondTableViewMill";
import SecondTableViewResource from "./SecondTableViewResource";
import SecondTableViewStockCordinate from "./SecondTableViewStockCordinate";
import SecondTableViewMillCordinate from "./SecondTableViewMillCordinate";
import { useDispatch, useSelector } from "react-redux";
import { updateSecondCurrPreview } from "../../../../store/Features/importReducer";

const SecondTabView = () => {

  let importItems = useSelector((store) => store.import);
  const dispatch = useDispatch();
  //set value from reducer
  const [value, setValue] = React.useState("1");
  const handleChange = (event, newValue) => {
    console.log('newvalue', newValue);
    setValue(newValue);
    dispatch(updateSecondCurrPreview(newValue));
  };

  useEffect(() => {
    console.log(importItems.secondCurrPreview, 'importItems.secondCurrPreview');
    setValue(importItems.secondCurrPreview);
    dispatch(updateSecondCurrPreview(importItems.secondCurrPreview));
  }, [importItems.secondCurrPreview])

  return (
    <div style={{ height: '100vh', overflow: 'auto' }}>
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <TabList
            className="rightPrevTabList"
            onChange={handleChange}
            aria-label="lab API tabs example"
          >
            <Tab label="Stock Pile" value="1" />
            <Tab label="Mill" value="2" />
            <Tab label="Resource" value="3" />
          </TabList>
        </Box>
        <TabPanel className="w-100" value="1" style={{ padding: "0" }}>
          <SecondTableViewStock />
          <SecondTableViewStockCordinate />
        </TabPanel>
        <TabPanel className="w-100" value="2" style={{ padding: "0" }}>
          <SecondTableViewMill />
          <h3>Min Capacity</h3>
          <SecondTableViewMillCordinate min />
          <h3>Max Capacity</h3>
          <SecondTableViewMillCordinate max />
        </TabPanel>
        <TabPanel className="w-100" value="3" style={{ padding: "0" }}>
          <SecondTableViewResource />
        </TabPanel>
      </TabContext>
    </div>
  );
};

export default SecondTabView;
