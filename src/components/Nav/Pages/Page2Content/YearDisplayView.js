import React from "react";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { useSelector, useDispatch } from "react-redux";
import { updateCurrYear } from "../../../../store/Features/importReducer";
import { Label } from "@material-ui/icons";
const yearList = [
  { value: 1, title: "1" },
  { value: 2, title: "2" },
  { value: 3, title: "3" },
  { value: 4, title: "4" },
  { value: 5, title: "5" },
  { value: 6, title: "6" },
  { value: 7, title: "7" },
  { value: 8, title: "8" },
  { value: 9, title: "9" },
  { value: 10, title: "10" },
  { value: 11, title: "11" },
  { value: 12, title: "12" },
  { value: 13, title: "13" },
  { value: 14, title: "14" },
  { value: 15, title: "15" },
];

const YearDisplayView = () => {
  let importItems = useSelector((store) => store.import);
  const dispatch = useDispatch();
  const [age, setAge] = React.useState("");

  const handleChange = (event) => {
    setAge(event.target.value);
    dispatch(updateCurrYear(event.target.value));
  };

  return (
    <div style={{ padding: "20px" }}>
      <FormControl sx={{ m: 1, minWidth: 120 }} size="small">
        <label>Year</label>
        <Select
          labelId="demo-select-small"
          id="demo-select-small"
          value={importItems.currYear}
          onChange={handleChange}
        >
          {yearList.map((item, index) => (
            <MenuItem key={index} value={item.value}>{item.title}</MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
};

export default YearDisplayView;
