import React from "react";
import pitImage from "../../../../assets/images/Pit.png";
import dumpImage from "../../../../assets/images/Dump.png";
import stockPileImage from "../../../../assets/images/Cordinate.png";
import processPlantImage from "../../../../assets/images/Road.png";
import { useSelector, useDispatch } from "react-redux";
import {
  removeProcessPlant,
  removeStockPile,
} from "../../../../store/Features/importReducer";

const MiddlePanelImage = (props) => {
  console.log('heavy', props);
  let importItems = useSelector((store) => store.import);
  const dispatch = useDispatch();
  const imageStyle = {
    top: Math.random() * 50 + "%",
    left: Math.random() * 50 + "%",
  };
  switch (props?.imageRender) {
    case "pit":
      return (
        <div className="imageContainer" >
          <img className="middlePanelImage" src={pitImage} alt="pitImage" />
          {/* <p>{props?.data ? `${props?.data.name}` : ``}</p> */}
        </div>
      );
    case "dump":
      return (
        <div className="imageContainer">
          <img className="middlePanelImage" src={dumpImage} alt="dumpImage" />
          <p>{props?.data ? `${props?.data.name} [${props?.data.type}]` : ``}</p>
        </div>
      );
    case "processPlant":
      return (
        <div
          className="imageContainer"
          onDoubleClick={() => {
            dispatch(removeProcessPlant(props.data.uid));
          }}
        >
          <img
            className="middlePanelImage"
            src={processPlantImage}
            alt="processPlantImage"
          />
          <p>{props?.data ? `${props?.data.name}` : ``}</p>
        </div>
      );
    case "stockPile":
      return (
        <div
          className="imageContainer"
        // onDoubleClick={() => {
        //   dispatch(removeStockPile(props.data.uid));
        // }}
        >
          <img
            className="middlePanelImage"
            src={stockPileImage}
            alt="stockPileImage"
          />
          <p>{props?.data ? `${props?.data.name}` : ``}</p>
        </div>
      );
    // case "road":
    //   return (
    //     <div className="middlePanelImage roadImage" style={imageStyle}></div>
    //   );

    default:
      return;
  }
};

export default MiddlePanelImage;
