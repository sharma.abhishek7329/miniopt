import React from "react";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Divider from "@mui/material/Divider";
import LeftPanel from "./Page1Content/LeftPanel";
import MiddlePanel from "./Page1Content/MiddlePanel";
import RightPanel from "./Page1Content/RightPanel";
import DataPreviewModal from "./Page1Content/DataPreviewModal";
import { useSelector, useDispatch } from "react-redux";
import SecondLeftPanel from "./Page2Content/SecondLeftPanel";
import SecondRightPanel from "./Page2Content/SecondRightPanel";
const Page1 = (props) => {
  let modalItems = useSelector((store) => store.modal);
  const dispatch = useDispatch();

  return (
    <div style={{ height: "100%" }}>
      <Grid container>
        <Grid item xs={2} style={{ marginTop: "0px !important" }}>
          {props.allowMiddlewindow ? <SecondLeftPanel /> : <LeftPanel />}
        </Grid>
        <Divider orientation="vertical" flexItem />
        <Grid item xs>
          <MiddlePanel allowMiddlewindow={props.allowMiddlewindow} />
        </Grid>
        <Divider orientation="vertical" flexItem />
        <Grid item xs={6}>
          {props.allowRightPanel ? <SecondRightPanel /> : <RightPanel />}
        </Grid>
      </Grid>

      {modalItems.isDataModal && <DataPreviewModal />}
    </div>
  );
};

export default Page1;
