import React from "react";
import SwipeableViews from "react-swipeable-views";
import { useTheme } from "@mui/material/styles";
import AppBar from "@mui/material/AppBar";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Page1 from "./Pages/Page1";
import Page2 from "./Pages/Page2";
import Page3 from "./Pages/Page3";
import Page4 from "./Pages/Page4";
import TabContext from "@mui/lab/TabContext";
import "./Nav.css";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import { styled } from "@mui/material/styles";
import Button from "@mui/material/Button";
import Tooltip, { tooltipClasses } from "@mui/material/Tooltip";
import image1 from "../../assets/images/1.png";
import image2 from "../../assets/images/2.png";
import image3 from "../../assets/images/3.png";
import image4 from "../../assets/images/4.png";
const LightTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: theme.palette.common.white,
    color: "rgba(0, 0, 0, 0.87)",
    boxShadow: theme.shadows[1],
    fontSize: 11,
  },
}));

const Nav = () => {
  const [value, setValue] = React.useState("1");

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <>
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <TabList
            onChange={handleChange}
            aria-label="lab API tabs example"
            style={{ backgroundColor: "#5DA3FA" }}
          >
            <LightTooltip title="Add pit dump data" placement="right">
              <Tab
                className={value === "1" ? "activeTab" : ""}
                icon={<img className="headerImage" src={image1} />}
                value="1"
              />
            </LightTooltip>
            <LightTooltip title="Add Stockpile plant data" placement="right">
              <Tab
                className={value === "2" ? "activeTab" : ""}
                icon={<img className="headerImage" src={image2} />}
                value="2"
              />
            </LightTooltip>
            <LightTooltip title="Add cost Price data" placement="right">
              <Tab
                className={value === "3" ? "activeTab" : ""}
                icon={<img className="headerImage" src={image3} />}
                value="3"
              />
            </LightTooltip>
            <LightTooltip title="Reports" placement="right">
              <Tab
                className={value === "4" ? "activeTab" : ""}
                icon={<img className="headerImage" src={image4} />}
                value="4"
              />
            </LightTooltip>
          </TabList>
        </Box>
        <TabPanel value="1">
          <Page1 />
        </TabPanel>
        <TabPanel value="2">
          <Page2 />
        </TabPanel>
        <TabPanel value="3">
          <Page3 />
        </TabPanel>
        <TabPanel value="4">
          <Page4 />
        </TabPanel>
      </TabContext>
    </>
  );
};

export default Nav;
