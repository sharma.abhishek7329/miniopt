import React from "react";
import DocumentScannerIcon from "@mui/icons-material/DocumentScanner";

export const NoFileAdded = () => {
  return (
    <div className="no-document">
      <DocumentScannerIcon sx={{ fontSize: 30 }} color="primary" />
      <h4>File not added</h4>
    </div>
  );
};
