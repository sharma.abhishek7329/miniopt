import * as React from "react";
import * as ReactDOM from "react-dom";
import { App } from "./components/App/App";

function render() {
  ReactDOM.render(<h2>From React</h2>, document.body);
}

render();
