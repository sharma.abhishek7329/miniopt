import { configureStore } from "@reduxjs/toolkit";
import importReducer from "./Features/importReducer";
import modalReducer from "./Features/modalReducer";

export const store = configureStore({
  reducer: {
    import: importReducer,
    modal: modalReducer,
  },
});

// // Infer the `RootState` and `AppDispatch` types from the store itself
// export type RootState = ReturnType<typeof store.getState>
// // Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
// export type AppDispatch = typeof store.dispatch
