import { createSlice, PayloadAction } from "@reduxjs/toolkit";

const initialState = {
  isDataModal: false,
};

export const modalSlice = createSlice({
  name: "import",
  initialState,
  reducers: {
    updateDataModal: (state, { payload }) => {
      state.isDataModal = payload;
    },
  },
});
export const { updateDataModal } = modalSlice.actions;
export default modalSlice.reducer;
