import { createSlice, PayloadAction } from "@reduxjs/toolkit";

const initialState = {
  pitList: [],
  cordinateList: [],
  dumpList: [],
  roadList: [],
  pits: [],
  cordinates: [],
  roads: [],
  dumps: [],
  stockPileList: [],
  processPlantList: [],
  currTab: '1',
  currPreview: "1",
  secondCurrPreview: "1",
  currYear: 1,
  stockPileData: [],
  stockPileCordinateData: [],
  millData: [],
  uniqueDumpData: [],
  millCordinateDataMin: [],
  millCordinateDataMax: [],
  resourceData: [],
};

export const importSlice = createSlice({
  name: "import",
  initialState,
  reducers: {
    updateCurrTab: (state, { payload }) => {
      state.currTab = payload;
    },
    updateCurrYear: (state, { payload }) => {
      state.currYear = payload;
    },
    updateCurrPreview: (state, { payload }) => {
      state.currPreview = payload;
    },
    updateSecondCurrPreview: (state, { payload }) => {
      state.secondCurrPreview = payload;
    },
    addStockPileList: (state, { payload }) => {
      state.stockPileList = [...state.stockPileList, payload];
    },
    addStockPileData: (state, { payload }) => {
      state.stockPileData = [payload];
    },
    addDumpData: (state, { payload }) => {
      console.log('dumpdata', payload);
      state.uniqueDumpData = payload
    },
    addStockPileCordinateData: (state, { payload }) => {
      state.stockPileCordinateData = [payload];
    },
    addMillData: (state, { payload }) => {
      state.millData = [payload];
    },
    addMillCordinateDataMin: (state, { payload }) => {
      state.millCordinateDataMin = [payload];
    },
    addMillCordinateDataMax: (state, { payload }) => {
      state.millCordinateDataMax = [payload];
    },
    addResourceData: (state, { payload }) => {
      state.resourceData = [payload];
    },
    addProcessPlantList: (state, { payload }) => {
      state.processPlantList = [...state.processPlantList, payload];
    },
    removeStockPile: (state, { payload }) => {
      state.stockPileList = state.stockPileList.filter(
        (item) => item.uid !== payload
      );
    },
    removeProcessPlant: (state, { payload }) => {
      state.processPlantList = state.processPlantList.filter(
        (item) => item.uid !== payload
      );
    },
    addPits: (state, { payload }) => {
      state.pits = [...state.pits, payload];
    },
    addPitList: (state, { payload }) => {
      state.pitList = [...state.pitList, payload[0]];
    },
    clearPitsList: (state) => {
      state.pitList = [];
    },
    removePitListItem: (state, { payload }) => {
      state.pitList = state.pitList.filter((item) => item.id !== payload);
    },
    addCordinates: (state, { payload }) => {
      state.cordinates = [...state.cordinates, payload];
    },
    addCordinateList: (state, { payload }) => {
      state.cordinateList = [...state.cordinateList, payload[0]];
    },
    clearCordinatesList: (state) => {
      state.cordinateList = [];
    },
    removeCordinateListItem: (state, { payload }) => {
      state.cordinateList = state.cordinateList.filter(
        (item) => item.id !== payload
      );
    },
    addDumps: (state, { payload }) => {
      state.dumps = [...state.dumps, payload];
    },
    addDumpList: (state, { payload }) => {
      state.dumpList = [...state.dumpList, payload[0]];
    },
    clearDumpsList: (state) => {
      state.dumpList = [];
    },
    removeDumpListItem: (state, { payload }) => {
      state.dumpList = state.dumpList.filter((item) => item.id !== payload);
    },
    addRoads: (state, { payload }) => {
      state.roads = [...state.roads, payload];
    },
    addRoadList: (state, { payload }) => {
      state.roadList = [...state.roadList, payload[0]];
    },
    clearRoadsList: (state) => {
      state.roadList = [];
    },
    removeRoadListItem: (state, { payload }) => {
      state.roadList = state.roadList.filter((item) => item.id !== payload);
    },
    addPitListToLast: (state, { payload }) => {
      state.pitList = [...state.pitList, payload[0]];
    },
    addCordinateListToLast: (state, { payload }) => {
      state.cordinateList = [...state.cordinateList, payload[0]];
    },
    addDumpListToLast: (state, { payload }) => {
      state.dumpList = [...state.dumpList, payload[0]];
    },
    addRoadListToLast: (state, { payload }) => {
      state.roadList = [...state.roadList, payload[0]];
    },
  },
});
export const {
  addPits,
  addRoads,
  addDumps,
  addCordinates,
  addPitList,
  clearRoadsList,
  clearPitsList,
  clearDumpsList,
  clearCordinatesList,
  addCordinateList,
  addDumpList,
  addRoadList,
  removePitListItem,
  removeCordinateListItem,
  removeDumpListItem,
  removeRoadListItem,
  updateCurrTab,
  updateCurrPreview,
  addPitListToLast,
  addCordinateListToLast,
  addRoadListToLast,
  addDumpListToLast,
  addStockPileList,
  addProcessPlantList,
  removeStockPile,
  removeProcessPlant,
  updateCurrYear,
  addStockPileData,
  addMillData,
  addResourceData,
  addMillCordinateData,
  addStockPileCordinateData,
  addMillCordinateDataMin,
  addMillCordinateDataMax,
  updateSecondCurrPreview,
  addDumpData
} = importSlice.actions;
export default importSlice.reducer;
